package com.example.detruit.fitbody;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Workouts extends AppCompatActivity {

    Button Tips;
    Button Workouts;
    Button Diets;
    Button BackPage;
    Button MainMenü;
    Button FiveTimeFive;
    Button FullBody;
    Button Split;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workouts);
        Tips = findViewById(R.id.tipsButton);
        Workouts = findViewById(R.id.workoutButton);
        Diets = findViewById(R.id.dietButton);
        BackPage = findViewById(R.id.backPageButton);
        MainMenü = findViewById(R.id.mainMenuButton);
        FiveTimeFive = findViewById(R.id.fiveButton);
        FullBody = findViewById(R.id.fullButton);
        Split = findViewById(R.id.splitButton);

        MainMenü.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Workouts.this, MainActivity.class);
                startActivity(intent);
            }
        });
        Tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Workouts.this, Tips.class);
                startActivity(intent);
            }
        });
        Diets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Workouts.this, DietActivity.class);
                startActivity(intent);
            }
        });
        FiveTimeFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Workouts.this, fiveTimesFive.class);
                startActivity(intent);
            }
        });
        FullBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Workouts.this, FullBody.class);
                startActivity(intent);
            }
        });
        Split.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Workouts.this, Split.class);
                startActivity(intent);
            }
        });
        BackPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
