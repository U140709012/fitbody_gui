package com.example.detruit.fitbody;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DietActivity extends AppCompatActivity {

    Button Tips;
    Button Workouts;
    Button Diets;
    Button BackPage;
    Button MainMenü;
    Button Ketogenic;
    Button LowCarb;
    Button Vegetarian;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diet);
        Tips = (Button) findViewById(R.id.tipsButton);
        Workouts = findViewById(R.id.workoutButton);
        Diets = findViewById(R.id.dietButton);
        BackPage = findViewById(R.id.backPageButton);
        MainMenü = findViewById(R.id.mainMenuButton);
        Ketogenic = findViewById(R.id.KetogenicDietButton);
        LowCarb = findViewById(R.id.LowCarbDietButton);
        Vegetarian = findViewById(R.id.VegetarianDietButton);

        MainMenü.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DietActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        Ketogenic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DietActivity.this, Ketogenic.class);
                startActivity(intent);
            }
        });
        LowCarb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DietActivity.this, LowCarb.class);
                startActivity(intent);
            }
        });
        Vegetarian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DietActivity.this, Vegetarian.class);
                startActivity(intent);
            }
        });
        Workouts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DietActivity.this, Workouts.class);
                startActivity(intent);
            }
        });
        Tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DietActivity.this, Tips.class);
                startActivity(intent);
            }
        });
        BackPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
