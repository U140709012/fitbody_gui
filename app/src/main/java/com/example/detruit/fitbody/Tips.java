package com.example.detruit.fitbody;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Tips extends AppCompatActivity {

    TextView NewBmiText;
    TextView TipsText;
    Button Tips;
    Button Workouts;
    Button Diets;
    Button BackPage;
    Button MainMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);
        NewBmiText = (TextView) findViewById(R.id.NewBMIText);
        TipsText = (TextView) findViewById(R.id.TipsText);
        Tips = (Button) findViewById(R.id.tipsButton);
        Workouts = findViewById(R.id.workoutButton);
        Diets = findViewById(R.id.dietButton);
        BackPage = findViewById(R.id.backPageButton);
        MainMenu = findViewById(R.id.mainMenuButton);

        SharedPreferences sharedPref = getSharedPreferences("SaveBmi",MODE_PRIVATE);
        String BmiData = sharedPref.getString("Bmi","");

            NewBmiText.setText(BmiData);
            Double bmi = Double.parseDouble(NewBmiText.getText().toString());
            if (bmi < 18.5) {
                TipsText.setText(R.string.Tips_underweight);
            } else if (bmi >= 18.5 && bmi < 23) {
                TipsText.setText(R.string.Tips_healty);
            } else if (bmi >= 23 && bmi < 27.5) {
                TipsText.setText(R.string.Tips_overweight);
            } else {
                TipsText.setText(R.string.Tips_obese);
            }

        MainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tips.this, MainActivity.class);
                startActivity(intent);
            }
        });
        Diets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tips.this, DietActivity.class);
                startActivity(intent);
            }
        });
        Workouts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tips.this, Workouts.class);
                startActivity(intent);
            }
        });
        BackPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


}
