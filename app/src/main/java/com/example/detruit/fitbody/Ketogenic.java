package com.example.detruit.fitbody;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Ketogenic extends AppCompatActivity {

    Button Tips;
    Button Workouts;
    Button Diets;
    Button BackPage;
    Button MainMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ketogenic);
        Tips = (Button) findViewById(R.id.tipsButton);
        Workouts = findViewById(R.id.workoutButton);
        Diets = findViewById(R.id.dietButton);
        BackPage = findViewById(R.id.backPageButton);
        MainMenu = findViewById(R.id.mainMenuButton);

        MainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Ketogenic.this, MainActivity.class);
                startActivity(intent);
            }
        });
        Diets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Ketogenic.this, DietActivity.class);
                startActivity(intent);
            }
        });
        Workouts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Ketogenic.this, Workouts.class);
                startActivity(intent);
            }
        });
        Tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Ketogenic.this, Tips.class);
                startActivity(intent);
            }
        });
        BackPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
