package com.example.detruit.fitbody;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    RadioGroup radioGroup;
    Button BttnCalculateBMI;
    Button BttnGetstarted;
    EditText HeightTxt;
    EditText WeightTxt;
    TextView BMItxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioGroup = findViewById(R.id.FMG);
        BttnCalculateBMI = (Button) findViewById(R.id.CalculateButton);
        BttnGetstarted = (Button) findViewById(R.id.StartButton);
        HeightTxt = (EditText) findViewById(R.id.HeightText);
        WeightTxt = (EditText) findViewById(R.id.WeightText);
        BMItxt = (TextView) findViewById(R.id.BMIText);

        //Bmi Calculation
        BttnCalculateBMI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    final Double height = Double.parseDouble(HeightTxt.getText().toString());
                    final Double weight = Double.parseDouble(WeightTxt.getText().toString());
                    double Bmi;
                    Bmi = weight/(height * height);
                    String FinalBmi = String.valueOf(Bmi);
                    BMItxt.setText(FinalBmi);
                }catch (NumberFormatException ex){
                    double height = 1;
                    double weight = 0;
                    double Bmi;
                    Bmi = weight/(height * height);
                    String FinalBmi = String.valueOf(Bmi);
                    BMItxt.setText(FinalBmi);
                }
            }
        });


        // Shared Pref and Intent Commnand lines
        BttnGetstarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getSharedPreferences("SaveBmi",MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("Bmi",BMItxt.getText().toString());
                editor.apply();
                Intent intent = new Intent(MainActivity.this, Tips.class);
                startActivity(intent);

            }
        });


    }


}
